#Hacker News Clone - Sapient POC


*TechStack*
- React JS
- Redux
- Saga
- Axios
- react-router-dom
- ChartJS 2

*Commands*
- yarn install
- yarn start:dev
- yarn build 

*Gitlab Pipeline Stages* 
- install [yarn install]
- build [yarn build-client]
- test [yarn test]
- deploy [deploy on heroku]

Heroku URL = https://heacker-rank-sapient-poc.herokuapp.com/
