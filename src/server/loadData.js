import "isomorphic-fetch";

import  memoize from "memoizee";

const fetchData = async page => {
    return fetch("https://hn.algolia.com/api/v1/search_by_date?page=" + page)
        .then(response => response.json())
        .catch(error => console.error(error));
};

export default memoize(fetchData);
