import {applyMiddleware, compose, createStore} from 'redux';
import createSagaMiddleware, {END} from 'redux-saga';
import {routerMiddleware} from 'react-router-redux';

import reducers from '../shared/reducers';

const sagaMiddleware = createSagaMiddleware();

const reduxMiddlewares = [
    sagaMiddleware,
];

export default (initialState) => {
    const store = createStore(
        reducers,
        initialState,
        compose(applyMiddleware(...reduxMiddlewares)),
    );

    store.runSaga = sagaMiddleware.run;

    store.close = () => store.dispatch(END);

    return store;
};
