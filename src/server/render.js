export const renderHeader = () => `
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
               <title>Hacker News - Sapient POC</title>
            <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
            <link rel="icon" type="image/png" href="/assets/favicon.ico" />
             <link href="/assets/bundle.css" rel="stylesheet"/>
        </head>
        <body>
            <div id="root">
`;

export const renderFooter = () => `
            </div>
            <script src="/assets/vendor.js"></script>
            <script src="/assets/client.js"></script>
        </body>
    </html>
`;
