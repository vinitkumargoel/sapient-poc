/* eslint-disable no-console */
import express from 'express';
import React from 'react';
import {renderToNodeStream} from 'react-dom/server';
import {Provider} from 'react-redux';
import compression from "compression";
import App from '../shared/App';
import configureStore from './store';
import {renderFooter, renderHeader} from './render';
import sagas from '../shared/sagas';
import loadData from "./loadData";
import logger from "morgan";

const app = express();
app.use(logger("dev"));
app.use('/assets', express.static('./dist'));
app.use(compression());
app.get(['/', "/:page"], async (req, res) => {
    let {page = 1} = req.params;
    let response = {};
    if (!isNaN(page)) {
        response = await loadData(page);
    }
    const store = configureStore();
    const context = {};
    const appWithRouter = (
        <Provider store={store}>
            <App existing={response}/>
        </Provider>
    );
    if (context.url) {
        res.redirect(context.url);
        return;
    }

    store.runSaga(sagas).done.then(() => {
        res.status(200).write(renderHeader());
        const htmlSteam = renderToNodeStream(appWithRouter);
        htmlSteam.pipe(res, {end: false});
        htmlSteam.on('end', () => {
            res.write(renderFooter());
            return res.send();
        });
    });
    store.close();
});

app.listen(process.env.PORT || 3000, () => console.log('Demo app listening on port 3000'));
