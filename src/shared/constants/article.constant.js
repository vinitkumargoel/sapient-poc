export const ARTICLE_GET_ALL = "article/get-all";
export const ARTICLE_GET_ALL_SUCCESS = "article/get-all/success";
export const ARTICLE_GET_ALL_ERROR = "article/get-all/error";

export const ARTICLE_INITIAL_HIDE = "article/initial-hide";
export const ARTICLE_HIDE = "article/hide";
export const ARTICLE_HIDE_SUCCESS = "article/hide/success";
