export const UPVOTE_GIVE = "upvote/give";
export const UPVOTE_GIVE_SUCCESS = "upvote/give/success";
export const UPVOTE_GIVE_ERROR = "upvote/give/error";
export const UPVOTE_GET_INITIAL = "upvote/give-initial";
