import React, {useEffect} from 'react';
import {connect} from "react-redux";
import "./index.css";
import Header from "../../components/Header";
import {ARTICLE_GET_ALL, ARTICLE_INITIAL_HIDE} from "../../constants/article.constant";
import {UPVOTE_GET_INITIAL} from "../../constants/upvote.constant";
import TableRow from "../TableRow/TableRow";
import Chart from "../Chart/Chart";
import Pagination from "../../components/Pagination";

const LandingPage = ({data, existing, ...props}) => {
    let page = 1;
    if (typeof window !== "undefined") {
        page = window.location.pathname.replace("/", "") || 1;
    }
    if (existing && existing.hits) {
        data = existing.hits;
    }
    useEffect(() => {
        props.getUpVotes();
        props.getHidden();
    }, []);
    useEffect(() => {
        props.getArticles(page);
    }, [page]);
    return (
        <div className='landing-page'>
            <div className="container">
                <div className="row">
                    <table className="table">
                        <Header/>
                        <tbody>
                        {data.map((a, i) => <TableRow data={a} key={i}/>)}
                        </tbody>
                    </table>
                    <Pagination/>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <Chart/>
                    </div>
                </div>
            </div>
        </div>
    );
};

function mapStateToProps({articles}) {
    return {
        data: articles.all.data || [],
    };
}

function mapDispatchToProps(dispatch) {
    return {
        getArticles: (page = 1) => dispatch({type: ARTICLE_GET_ALL, payload: {page}}),
        getUpVotes: () => dispatch({type: UPVOTE_GET_INITIAL}),
        getHidden: () => dispatch({type: ARTICLE_INITIAL_HIDE}),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LandingPage);
