import React from 'react';
import {connect} from "react-redux";
import {Line} from "react-chartjs-2";

function getUpVoteData({articles = [], upvote, hidden}) {
    let final = {};
    for (let i of articles) {
        if (i.story_title && !hidden[i.objectID]) {
            final[i.objectID] = upvote[i.objectID] || 0;
        }
    }
    return final;
}

const Chart = props => {
    let final = getUpVoteData(props);
    const data = () => {
        return {
            labels: Object.keys(final),
            datasets: [{
                label: '# of Votes',
                borderColor: "#ff6600",
                data: Object.values(final),
                borderWidth: 3,
                fill: false,
            }]
        }
    };
    return (
        <div>
            <Line data={data}/>
        </div>
    );
};

function mapStateToProps({articles, upvote}) {
    return {
        hidden: articles.hidden || {},
        upvote: upvote.all.data || {},
        articles: articles.all.data || {hits: []},
    };
}

function mapDispatchToProps(dispatch) {
    return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(Chart);
