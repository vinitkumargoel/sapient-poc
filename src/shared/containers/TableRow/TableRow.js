import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faCaretUp} from '@fortawesome/free-solid-svg-icons'
import "./index.css";
import moment from "moment";
import {connect} from "react-redux";
import {UPVOTE_GIVE} from "../../constants/upvote.constant";
import {ARTICLE_HIDE} from "../../constants/article.constant";

const TableRow = ({data = {}, ...props}) => {
    return (
        <React.Fragment>
            {data.story_title && !props.hidden[data.objectID] && <tr>
                <td className='text-center'>{data.num_comments || 0}</td>
                <td className='text-center'>{props.upVotes[data.objectID] || 0}</td>
                <td className='text-center'>
                    <button className='upvote-btn' onClick={() => props.giveUpvote(data.objectID)}>
                        <FontAwesomeIcon icon={faCaretUp} color={'#989898'}/>
                    </button>
                </td>
                <td>
                    {data.story_title}
                    <span className='light'>by</span>
                    {data.author}
                    <span className='light'>{moment(data.created_at).fromNow()}</span>
                    {data.url}
                    [ <button className='hide-button'
                              onClick={() => props.hideArticle(data.objectID)}>hide</button> ]
                </td>
            </tr>}
        </React.Fragment>
    );
};

function mapStateToProps({upvote, articles}) {
    return {
        hidden: articles.hidden || {},
        upVotes: upvote.all.data || [],
        articles: articles.all.data || [],
    };
}

function mapDispatchToProps(dispatch) {
    return {
        hideArticle: id => dispatch({type: ARTICLE_HIDE, payload: id}),
        giveUpvote: id => dispatch({type: UPVOTE_GIVE, payload: id}),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TableRow);
