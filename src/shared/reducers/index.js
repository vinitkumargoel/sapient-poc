import {combineReducers} from "redux";
import articles from "./article.reducer";
import upvote from "./upvote.reducer";

export default combineReducers({
    articles,
    upvote,
});
