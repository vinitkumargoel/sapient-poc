import {UPVOTE_GIVE, UPVOTE_GIVE_ERROR, UPVOTE_GIVE_SUCCESS} from "../constants/upvote.constant";

const initialState = {
    all: {
        loading: false,
        data: {},
        error: {},
    }
};

export default (state = initialState, action) => {
    switch (action.type) {
        case UPVOTE_GIVE:
            return {
                ...state,
                all: {
                    loading: true,
                    data: {},
                    error: {},
                }
            };
        case UPVOTE_GIVE_SUCCESS:
            return {
                ...state,
                all: {
                    loading: false,
                    data: action.payload,
                    error: {},
                }
            };
        case UPVOTE_GIVE_ERROR:
            return {
                ...state,
                all: {
                    loading: false,
                    data: {},
                    error: action.payload,
                }
            };
        default: {
            return state;
        }
    }
};
