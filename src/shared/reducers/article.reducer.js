import {
    ARTICLE_GET_ALL,
    ARTICLE_GET_ALL_ERROR,
    ARTICLE_GET_ALL_SUCCESS,
    ARTICLE_HIDE_SUCCESS
} from "../constants/article.constant";

const initialState = {
    all: {
        loading: false,
        data: [],
        error: {},
    },
    hidden: {}
};

export default (state = initialState, action) => {
    switch (action.type) {
        case ARTICLE_GET_ALL:
            return {
                ...state,
                all: {
                    loading: true,
                    data: [],
                    error: {},
                }
            };
        case ARTICLE_GET_ALL_SUCCESS:
            return {
                ...state,
                all: {
                    loading: false,
                    data: action.payload,
                    error: {},
                }
            };
        case ARTICLE_GET_ALL_ERROR:
            return {
                ...state,
                all: {
                    loading: false,
                    data: [],
                    error: action.payload,
                }
            };
        case ARTICLE_HIDE_SUCCESS:
            return {
                ...state,
                hidden: action.payload,
            };
        default: {
            return state;
        }
    }
};
