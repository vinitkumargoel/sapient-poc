import React from 'react';
import Header from './Header';
import enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

enzyme.configure({adapter: new Adapter()});
describe('Header Component', function () {
    const container = enzyme.shallow(<Header/>);
    it('should render with correct html', function () {
        expect(container.find('thead').length).toEqual(1);
        expect(container.find('tr').length).toEqual(1);
        expect(container.find('th').length).toEqual(4);
    });
});
