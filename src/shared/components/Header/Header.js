import React from 'react';
import "./index.css";

const Header = props => {
    return (
        <thead className='header'>
        <tr>
            <th className='text-center comments-head'>Comments</th>
            <th className='text-center vote-head'>Vote Count</th>
            <th className='text-center upvote-head'>UpVote</th>
            <th>News Details</th>
        </tr>
        </thead>
    );
};

Header.propTypes = {};

export default Header;
