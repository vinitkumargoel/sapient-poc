import React from 'react';

const Pagination = props => {
    let page = 1;
    if (typeof window !== "undefined") {
        page = window.location.pathname.replace("/", "") || 1;
    }
    return (
        <div className="navigation">
            {Number(page) !== 1 && <a href={`/${Number(page) - 1}`}>Previous</a>}
            <a href={`/${Number(page) + 1}`}>Next</a>
        </div>
    );
};

export default Pagination;
