import React from 'react';
import Pagination from './Pagination';
import enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

enzyme.configure({adapter: new Adapter()});
describe('Pagination Component', function () {
    const container = page => enzyme.shallow(<Pagination page={page}/>);
    it('should render with correct pagination', function () {
        expect(container(1).find('div.navigation').length).toEqual(1);
    });
});
