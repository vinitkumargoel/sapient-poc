import React from "react";
import LandingPage from "./containers/LandingPage";
import "bootstrap/dist/css/bootstrap.min.css"

function App(props) {
    return (
        <LandingPage existing={props.existing}/>
    );

}

export default App;
