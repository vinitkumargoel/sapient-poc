import {all, fork} from 'redux-saga/effects';
import {getArticlesWatch, hideArticlesWatch, initialArticlesWatch} from "./article.saga";
import {getInitialWatch, giveUpvoteWatch} from "./upvote.saga";

export default function* rootSaga() {
    yield all([
        fork(getArticlesWatch),
        fork(giveUpvoteWatch),
        fork(getInitialWatch),
        fork(hideArticlesWatch),
        fork(initialArticlesWatch)
    ])
};
