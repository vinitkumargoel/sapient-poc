import {put, takeLatest} from 'redux-saga/effects';
import {UPVOTE_GET_INITIAL, UPVOTE_GIVE, UPVOTE_GIVE_ERROR, UPVOTE_GIVE_SUCCESS} from "../constants/upvote.constant";


function* giveUpvote(action) {
    try {
        let existing = window.localStorage.getItem("upvotes") || "{}";
        existing = JSON.parse(existing);
        if (existing[action.payload]) {
            existing[action.payload] = existing[action.payload] + 1
        } else {
            existing[action.payload] = 1;
        }
        window.localStorage.setItem('upvotes', JSON.stringify(existing));
        yield put({
            type: UPVOTE_GIVE_SUCCESS,
            payload: existing,
        });
    } catch (error) {
        yield put({
            type: UPVOTE_GIVE_ERROR,
            payload: {},
        });
    }
}

export function* giveUpvoteWatch() {
    yield takeLatest(UPVOTE_GIVE, giveUpvote);
}


function* getInitial(action) {
    try {
        let existing = window.localStorage.getItem("upvotes") || "{}";
        existing = JSON.parse(existing);
        window.localStorage.setItem('upvotes', JSON.stringify(existing));
        yield put({
            type: UPVOTE_GIVE_SUCCESS,
            payload: existing,
        });
    } catch (error) {
        yield put({
            type: UPVOTE_GIVE_ERROR,
            payload: {},
        });
    }
}

export function* getInitialWatch() {
    yield takeLatest(UPVOTE_GET_INITIAL, getInitial);
}

