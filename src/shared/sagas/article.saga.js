import axios from "axios";
import {put, takeLatest} from 'redux-saga/effects';
import {
    ARTICLE_GET_ALL,
    ARTICLE_GET_ALL_ERROR,
    ARTICLE_GET_ALL_SUCCESS,
    ARTICLE_HIDE,
    ARTICLE_HIDE_SUCCESS,
    ARTICLE_INITIAL_HIDE
} from "../constants/article.constant";


function* getArticles(action) {
    try {
        let res = yield axios.get(`https://hn.algolia.com/api/v1/search_by_date?page=${action.payload.page}`);
        yield put({
            type: ARTICLE_GET_ALL_SUCCESS,
            payload: res.data.hits,
        });
    } catch (error) {
        yield put({
            type: ARTICLE_GET_ALL_ERROR,
            payload: error,
        });
    }
}

export function* getArticlesWatch() {
    yield takeLatest(ARTICLE_GET_ALL, getArticles);
}


function* hideArticles(action) {
    try {
        let existing = window.localStorage.getItem("hidden") || "{}";
        existing = JSON.parse(existing);
        existing[action.payload] = true;
        window.localStorage.setItem('hidden', JSON.stringify(existing));
        yield put({
            type: ARTICLE_HIDE_SUCCESS,
            payload: existing,
        });
    } catch (error) {
        yield put({
            type: ARTICLE_HIDE_SUCCESS,
            payload: {},
        });
    }
}

export function* hideArticlesWatch() {
    yield takeLatest(ARTICLE_HIDE, hideArticles);
}


function* initialArticles(action) {
    try {
        let existing = window.localStorage.getItem("hidden") || "{}";
        existing = JSON.parse(existing);
        yield put({
            type: ARTICLE_HIDE_SUCCESS,
            payload: existing,
        });
    } catch (error) {
        yield put({
            type: ARTICLE_HIDE_SUCCESS,
            payload: {},
        });
    }
}

export function* initialArticlesWatch() {
    yield takeLatest(ARTICLE_INITIAL_HIDE, initialArticles);
}

