import {render} from 'react-dom';
import React, {Component} from 'react';
import {BrowserRouter as Router} from 'react-router-dom';
import {loadComponents} from 'loadable-components';
import {Provider} from 'react-redux';
import {applyMiddleware, compose, createStore} from 'redux';
import {ConnectedRouter, routerMiddleware} from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';
import createSagaMiddleware from 'redux-saga';

import App from '../shared/App';
import reducer from '../shared/reducers';
import sagas from '../shared/sagas';

// Grab the state from a global variable injected into the server-generated HTML
const preloadedState = window.__PRELOADED_STATE__;

const history = createHistory();
const sagaMiddleware = createSagaMiddleware();
// Allow the passed state to be garbage-collected
delete window.__PRELOADED_STATE__;

const store = createStore(
    reducer,
    preloadedState,
    compose(
        applyMiddleware(routerMiddleware(history), sagaMiddleware),
        window.devToolsExtension ? window.devToolsExtension() : f => f,
    ),
);

// then run the saga
sagaMiddleware.run(sagas);


class Main extends Component {
    // Remove the server-side injected CSS.
    componentDidMount() {
        const jssStyles = document.getElementById('jss-server-side');
        if (jssStyles && jssStyles.parentNode) {
            jssStyles.parentNode.removeChild(jssStyles);
        }
    }

    render() {
        return (
            <Router>
                <App {...this.props} />
            </Router>
        );
    }
}

loadComponents().then(() => {
    render(<Provider store={store}>
            <ConnectedRouter history={history}>
                <Main/>
            </ConnectedRouter>
        </Provider>,
        document.getElementById('root'),
    );
});
